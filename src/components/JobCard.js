// globals
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// styles

const getStyles = (isDetail) =>
  StyleSheet.create({
    jobCardWrapper: {
      borderWidth: 1,
      borderColor: isDetail ? '#4e37b2' : '#98979d50',
      backgroundColor: isDetail ? '#4e37b2' : 'transparent',
      borderRadius: 15,
      marginVertical: 8,
    },
    jobInfoWrapper: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
      padding: 24,
    },
    logo: {
      width: 70,
      height: 70,
      borderRadius: 20,
    },
    companyDetails: {
      paddingLeft: 32,
    },
    companyNameWrapper: {
      alignSelf: 'flex-start',
      borderRadius: 15,
      backgroundColor: isDetail ? '#604bb9' : '#ffe2d970',
    },
    company: {
      fontSize: 14,
      padding: 8,
      color: '#ff7751',
    },
    jobTitle: {
      fontSize: 16,
      fontWeight: 'bold',
      paddingVertical: 8,
      color: isDetail ? '#fbfbfb' : '#1e1a2d',
      letterSpacing: -0.5,
    },
    location: {
      fontSize: 14,
      color: '#98979d',
    },
    applyWrapper: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: 8,
    },
    leftSpace: {
      flex: 3,
    },
    roundWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      paddingRight: 8,
    },
    round: {
      width: 5,
      height: 5,
      borderRadius: 2.5,
      backgroundColor: 'black',
    },
    divider: {
      borderWidth: 1,
      borderStyle: 'dashed',
      borderColor: '#98979d30',
    },
    detailsWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      margin: 24,
      borderRadius: 16,
      paddingVertical: 16,
      paddingHorizontal: 24,
      backgroundColor: '#604bb9',
    },
    verticalDivider: {
      borderWidth: 0.3,
      height: 25,
      borderColor: '#9699d8',
    },
    subDetail: {
      color: '#fbfbfb80',
      fontSize: 14,
      paddingVertical: 4,
    },
    applyButton: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    applyText: {
      color: '#543eb5',
      fontSize: 18,
    },
    typeText: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#fbfbfb',
    },
  });

// component
const JobCard = (props) => {
  const {job, navigation, isDetail} = props;
  const {logo, company, title, location, posted, type, experience} = job;
  const styles = getStyles(isDetail);
  let width = 0;
  return (
    <View style={styles.jobCardWrapper}>
      <View style={styles.jobInfoWrapper}>
        <Image source={logo} style={styles.logo} />
        <View style={styles.companyDetails}>
          <View style={styles.companyNameWrapper}>
            <Text
              onLayout={(event) => {
                width = event.nativeEvent.layout.width;
              }}
              style={styles.company}>
              {company}
            </Text>
          </View>
          <Text style={styles.jobTitle}>{title}</Text>
          <Text style={styles.location}>{location}</Text>
        </View>
      </View>
      {isDetail ? (
        <View style={styles.detailsWrapper}>
          <View style={styles.subDetail}>
            <Text style={styles.subDetail}>Type</Text>
            <Text style={styles.typeText}>{type}</Text>
          </View>
          <View style={styles.verticalDivider} />
          <View>
            <Text style={styles.subDetail}>Experience</Text>
            <Text style={styles.typeText}>{experience + '+ years'}</Text>
          </View>
        </View>
      ) : (
        <>
          <View style={styles.divider} />
          <View style={styles.applyWrapper}>
            <View style={[styles.leftSpace, styles.roundWrapper]}>
              <View style={styles.round} />
            </View>
            <Text style={[styles.location, styles.leftSpace]}>{posted}</Text>
            <TouchableOpacity
              title="APPLY"
              onPress={() =>
                navigation.navigate('Job Detail', {
                  job: job,
                })
              }>
              <View style={styles.applyButton}>
                <Text style={[styles.jobTitle, styles.applyText]}>APPLY</Text>
                <Icon
                  name="chevron-right"
                  style={styles.chevronRight}
                  size={20}
                  color="#543eb5"
                />
              </View>
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
};

export default JobCard;
