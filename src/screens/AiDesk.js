import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const AiDesk = () => (
  <View style={styles.container}>
    <Text>AiDesk</Text>
  </View>
);
export default AiDesk;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
