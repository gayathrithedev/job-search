// globals
import React from 'react';
import {View, StyleSheet, TextInput, Text, ScrollView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// child components
import Welcome from '../components/Welcome';
import JobCard from '../components/JobCard';

// images
import apple from '../assets/apple.png';
import untappd from '../assets/untappd.png';
import paypal from '../assets/paypal.png';

// constants
const jobsList = [
  {
    title: 'Product Designer',
    company: 'Apple',
    location: 'Chennai, TN',
    posted: '5 Days ago',
    logo: apple,
    type: 'Full-Time',
    experience: 3,
    description:
      'We are looking for intelligent, innovative, and hardworking people who can add value as UI/UX designer.',
    responsibility: [
      'Conduct user search',
      'Visual designing, wireframing',
      'Build top class experience',
    ],
    skills: ['UI/UX Design', 'Research', 'Sketch'],
  },
  {
    title: 'Graphic / UI Designer',
    company: 'Untappd',
    location: 'Bangalore, India',
    posted: 'New',
    logo: untappd,
    type: 'Full-Time',
    experience: 5,
    description:
      'We are looking for intelligent, innovative, and hardworking people who can add value as UI/UX designer.',
    responsibility: [
      'Conduct user search',
      'Visual designing, wireframing',
      'Build top class experience',
    ],
    skills: ['UI/UX Design', 'Research', 'Sketch'],
  },
  {
    title: 'Sr. UX Designer',
    company: 'PayPal',
    location: 'Chennai, TN',
    posted: '3 Days ago',
    logo: paypal,
    type: 'Full-Time',
    experience: 2,
    description:
      'We are looking for intelligent, innovative, and hardworking people who can add value as UI/UX designer.',
    responsibility: [
      'Conduct user search',
      'Visual designing, wireframing',
      'Build top class experience',
    ],
    skills: ['UI/UX Design', 'Research', 'Sketch'],
  },
];

// component
const Jobs = ({navigation}) => (
  <SafeAreaView style={styles.wrapper} edges={['top', 'left', 'right']}>
    <ScrollView style={styles.wrapper}>
      <View style={[styles.wrapper, styles.container]}>
        <Welcome />
        <View style={styles.searchWrapper}>
          <View style={styles.inputWrapper}>
            <Icon
              name="magnify"
              size={24}
              color="#1a1e2d"
              style={styles.magnify}
            />
            <TextInput
              placeholder="Search for jobs"
              style={styles.searchInput}
              placeholderTextColor="#a5a5bb"
            />
          </View>
          <View style={styles.filter}>
            <Icon name="tune-vertical" size={24} color="#fff" />
          </View>
        </View>
        <View style={styles.jobListWrapper}>
          <Text style={styles.title}>Jobs for you:</Text>
          {jobsList.map((job) => (
            <JobCard
              key={job.title}
              job={job}
              isDetail={false}
              navigation={navigation}
            />
          ))}
        </View>
      </View>
    </ScrollView>
  </SafeAreaView>
);
export default Jobs;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingHorizontal: 24,
    paddingVertical: 16,
  },
  wrapper: {
    backgroundColor: '#fff',
  },
  searchWrapper: {
    paddingTop: 32,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  magnify: {
    paddingHorizontal: 4,
  },
  searchInput: {
    width: '80%',
    paddingHorizontal: 8,
    paddingVertical: 4,
    fontSize: 14,
  },
  inputWrapper: {
    width: '80%',
    backgroundColor: '#f4f3fb',
    borderRadius: 14,
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 10,
    fontSize: 14,
  },
  filter: {
    width: 45,
    height: 45,
    backgroundColor: '#ff7751',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  jobListWrapper: {
    paddingTop: 32,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 8,
  },
});
