// globals
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  Button,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MIcon from 'react-native-vector-icons/MaterialIcons';

// child component
import JobCard from '../components/JobCard';

// image
import profile from '../assets/dp.jpeg';

// style
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    padding: 24,
    backgroundColor: '#fff',
  },
  valuesWrapper: {
    paddingVertical: 8,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 8,
    paddingLeft: 8,
  },
  screenTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1e1a2d',
  },
  list: {
    paddingLeft: 24,
    lineHeight: 28,
    color: '#98979d',
    fontSize: 15,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 8,
    marginTop: -8,
  },
  skills: {
    paddingTop: 8,
    paddingLeft: 16,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  skillList: {
    padding: 8,
    backgroundColor: '#98979d20',
    borderRadius: 10,
    marginHorizontal: 8,
  },
  skillListText: {
    fontSize: 14,
    color: '#1e1a2d',
  },
  buttonWrapper: {
    paddingVertical: 30,
  },
  button: {
    fontSize: 14,
    backgroundColor: 'violet',
    color: 'white',
  },
  modal: {
    backgroundColor: '#fff',
    padding: 30,
    bottom: 0,
    position: 'absolute',
    borderRadius: 40,
    width: '90%',
  },
  profile: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 4,
    borderColor: '#fff',
    alignSelf: 'center',
    marginTop: -60,
  },
  modalContent: {
    flex: 1,
  },
  modalHeading: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingTop: 16,
    lineHeight: 28,
    textAlign: 'center',
  },
  notesInputWrapper: {
    marginVertical: 24,
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 16,
    borderColor: '#98979d50',
  },
  notesInput: {
    height: 170,
    paddingTop: 24,
    paddingBottom: 24,
    paddingHorizontal: 16,
    color: '#1e1a2d',
  },
  placeholderStyle: {
    fontSize: 16,
  },
  sendButton: {
    padding: 16,
    fontSize: 14,
    fontWeight: 'bold',
    borderRadius: 8,
    backgroundColor: '#543eb5',
    marginTop: 8,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
  wordsLength: {
    fontSize: 12,
    textAlign: 'right',
    paddingRight: 16,
    paddingBottom: 8,
    color: '#98979d',
  },
  subtitleWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  listText: {
    paddingLeft: 24,
  },
  modalButton: {
    bottom: 0,
  },
  lookMoreText: {
    fontSize: 16,
    color: '#98979d',
    paddingTop: 16,
    paddingBottom: 32,
    lineHeight: 23,
    textAlign: 'center',
  },
  closeIcon: {
    alignSelf: 'flex-end',
  },
  lineWrapper: {
    alignItems: 'flex-end',
  },
  line: {
    height: 5,
    borderRadius: 2,
    backgroundColor: '#98979d50',
    marginVertical: 2,
  },
  lineOuter: {
    width: 20,
  },
  lineInner: {
    width: 30,
  },
  successIconsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  feedWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    padding: 8,
    borderWidth: 3,
    borderColor: '#ff7751',
  },
  circle: {
    backgroundColor: '#98979d50',
    width: 16,
    height: 16,
    borderRadius: 7.5,
    marginBottom: 5,
  },
  buttonLoader: {
    backgroundColor: '#4e37b2',
    width: 25,
    height: 8,
    borderRadius: 3,
    marginTop: 5,
  },
  infoLoader: {
    height: 3,
    borderRadius: 2,
    backgroundColor: '#98979d50',
    marginVertical: 1,
  },
  moreInfoLoader: {
    width: 30,
  },
  nameInfoLoader: {
    width: 20,
  },
});

// component
const JobDetails = ({route, navigation}) => {
  const {job} = route.params;
  const {description, skills, responsibility} = job;
  const [openModal, setModalVisible] = React.useState(false);
  const [openSuccessMessage, setSuccessMessageVisible] = React.useState(false);
  const [note, setNote] = React.useState(null);
  const length = note ? note.length : 0;

  const renderSubtitle = (name, subtitle) => (
    <View style={styles.subtitleWrapper}>
      <Icon name={name} size={20} color="#98979d" />
      <Text style={styles.heading}>{subtitle + ':'}</Text>
    </View>
  );

  return (
    <SafeAreaView style={styles.wrapper} edges={['top', 'left', 'right']}>
      <ScrollView style={styles.wrapper}>
        <View style={styles.container}>
          <Modal
            isVisible={openModal}
            style={styles.modal}
            onBackdropPress={() => setModalVisible(false)}>
            <View style={styles.modalContent}>
              {openSuccessMessage ? (
                <Icon
                  name="close"
                  onPress={() => setModalVisible(false)}
                  size={24}
                  style={styles.closeIcon}
                  color="#98979d"
                />
              ) : null}
              {openSuccessMessage ? (
                <View>
                  <View style={styles.successIconsWrapper}>
                    <View style={styles.lineWrapper}>
                      <View style={[styles.line, styles.lineOuter]} />
                      <View style={[styles.line, styles.lineInner]} />
                      <View style={[styles.line, styles.lineInner]} />
                      <View style={[styles.line, styles.lineOuter]} />
                    </View>
                    <View style={styles.feedWrapper}>
                      <View style={styles.circle} />
                      <View
                        style={[styles.infoLoader, styles.nameInfoLoader]}
                      />
                      <View
                        style={[styles.infoLoader, styles.moreInfoLoader]}
                      />
                      <View style={styles.buttonLoader} />
                    </View>
                  </View>
                  <Text style={styles.modalHeading}>
                    Your job application has been sent successfully!
                  </Text>
                  <Text style={styles.lookMoreText}>
                    Look out for other interesting job openings that matches
                    your interest
                  </Text>
                </View>
              ) : (
                <View>
                  <Image source={profile} style={styles.profile} />
                  <Text style={styles.modalHeading}>
                    Personalize your application and send it to Selena Gomez
                  </Text>
                  <View style={styles.notesInputWrapper}>
                    <TextInput
                      placeholder="Add your notes here"
                      placeholderStyle={styles.placeholderStyle}
                      placeholderTextColor="#1e1a2d90"
                      multiline={true}
                      style={styles.notesInput}
                      onChangeText={(v) => setNote(v)}
                      maxLength={150}
                    />
                    <Text style={styles.wordsLength}>
                      {length + '/150 Words'}
                    </Text>
                  </View>
                </View>
              )}
              <TouchableOpacity
                onPress={() => {
                  if (openSuccessMessage) {
                    setModalVisible(false);
                    setNote(null);
                  } else {
                    setSuccessMessageVisible(!openSuccessMessage);
                  }
                }}
                disabled={note === null && !openSuccessMessage}
                style={[styles.sendButton, styles.modalButton]}>
                <Text style={styles.buttonText}>
                  {openSuccessMessage ? 'DONE' : 'SEND APPLICATION'}
                </Text>
              </TouchableOpacity>
            </View>
          </Modal>
          <View style={styles.header}>
            <Icon
              name="arrow-left"
              size={24}
              onPress={() => navigation.goBack(null)}
            />
            <Text style={styles.screenTitle}>Job Detail</Text>
            <Icon
              name="bookmark-outline"
              size={20}
              color="#1e1a2d"
              style={styles.icon}
            />
          </View>
          <JobCard job={job} isDetail={true} navigation={navigation} />
          <View style={styles.valuesWrapper}>
            {renderSubtitle('alpha-o-box-outline', 'DESCRIPTION')}
            <Text style={styles.list}>{description}</Text>
          </View>
          <View style={styles.valuesWrapper}>
            {renderSubtitle('clipboard-check-outline', 'RESPONSIBILITY')}
            {responsibility.map((item) => (
              <View
                key={item}
                style={[styles.subtitleWrapper, styles.listText]}>
                <MIcon name="double-arrow" color="#51ccb9" size={15} />
                <Text style={styles.list}>{item}</Text>
              </View>
            ))}
          </View>
          <View style={styles.valuesWrapper}>
            {renderSubtitle('head-lightbulb-outline', 'SKILLS')}
            <View style={styles.skills}>
              {skills.map((item) => (
                <View style={styles.skillList} key={item}>
                  <Text key={item} style={styles.skillListText}>
                    {item}
                  </Text>
                </View>
              ))}
            </View>
          </View>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity
              title="APPLY NOW"
              style={styles.sendButton}
              onPress={() => setModalVisible(true)}>
              <Text style={styles.buttonText}>APPLY NOW</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default JobDetails;
