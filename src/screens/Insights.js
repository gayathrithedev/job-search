import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const insights = () => (
  <View style={styles.container}>
    <Text>insights</Text>
  </View>
);
export default insights;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
